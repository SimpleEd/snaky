// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;

	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(2);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	SetActorTickInterval(MovementSpeed);
	UE_LOG(LogTemp, Warning, TEXT("%f"), MovementSpeed);
	MovementSpeed = FMath::Clamp(MovementSpeed, 0.2f, 10.0f);

}

void ASnakeBase::AddSnakeElement(int ElementsNum) 
{
	for (int i = 0; i < ElementsNum; i++) 
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SetActorHiddenInGame(true);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		
		FVector currentScale = GetActorScale3D();
		FVector newScale = currentScale * 7.01f * GetWorld()->GetDeltaSeconds();
		SetActorScale3D(newScale);

		if (ElemIndex == 0) 
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move() {
	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--) 
	{
		SnakeElements[0]->SetActorHiddenInGame(false);
		SnakeElements[i]->SetActorHiddenInGame(false);
		auto CurrentElement = SnakeElements[i];
		const auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other) {
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractavleInterface = Cast<IInteractable>(Other);
		if (InteractavleInterface) 
		{
			InteractavleInterface->Interact(this, bIsFirst);		
		}
	}
}

float ASnakeBase::SetMovementSpeed()
{
	MovementSpeed -= 0.04f;
	return 0.0f;
}

float ASnakeBase::GetMovementSpeed()
{
	return MovementSpeed;
}

